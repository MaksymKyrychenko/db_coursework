const ChartJsImage = require('chartjs-to-image');
const {Sequelize, Op} = require('sequelize');
const readlineSync = require('readline-sync');
const fs = require('fs');
const brain = require('brain.js');
const data = require('./CreateRandomData');
const faker = require('faker');
const exec = require('child_process').execSync;
const chart = require('chart.js');
const {Faculty,Student,Speciality,Group,Subject,Teacher,Mark,TeacherSubjects, GroupTeachers} = require('./Models');

const sequelize = new Sequelize('Students', 'postgres', 'Max095433', {
    host: 'localhost',
    dialect: 'postgres'
    
});

async function TrainNetwork()
{
    let students = await Student.findAll({raw: true, limit: 10000});
    let marks = await Mark.findAll({where: {student_id: {[Op.between]: [1, 10000]}}, raw: true});
    let dataOnTrain = [];
    for(let st of students)
    {
        for(let m of marks)
        {
            if(st.student_id == m.student_id)
            {
                let t = await GroupTeachers.findAll({where: {subject_id: m.subject_id, group_id: st.group_id}, raw: true});
                let t_id = t[0].teacher_id; 
                let teacher = await Teacher.findAll({where: {teacher_id: t_id}, raw: true});
                let tg = 0;
                if(teacher[0].academic_degree == 'Senior Instructor')
                {
                    tg = 1;
                }
                if(teacher[0].academic_degree == 'Master Instructor')
                {
                    tg = 2;
                }
                if(teacher[0].academic_degree == 'Assistant Professor')
                {
                    tg = 3;
                }
                if(teacher[0].academic_degree == 'Associate Professor')
                {
                    tg = 4;
                }
                if(teacher[0].academic_degree == 'University Professor')
                {
                    tg = 5;
                }
                dataOnTrain.push({input: {a: st.age / 100, b: st.graduate_score / 200, c: tg / 10}, output: {d: m.mark / 100}});
                console.log(`${st.age / 100}, ${st.graduate_score / 200}, ${tg / 10}, ${m.mark / 100}`);
            }
        }
    }
    const config = {
        iterations: 20000,
        log: true,
        logPeriod: 1,
        errorThresh: 0.00001
    };
    fs.writeFileSync('testfile.dat', 'yes, you can');
    const network = new brain.NeuralNetwork();
    console.log('NN is training');
    network.train(dataOnTrain, config);

    const json = network.toJSON();
    let jsonStr = JSON.stringify(json);

    fs.writeFileSync('trained-network-marks1.json', jsonStr);
}

async function addRandomData(quantity)
{
    const GroupsOnInsert = [];
    let prevMaxId = await Group.max('group_id');
    for(let i = 0; i < quantity; i++)
    {
        let groupCode = faker.random.alpha({count: 2, upcase: true}) + ' - ' + faker.random.number({min: 101, max: 499});
        let group = {group_id: prevMaxId + i + 1, group_name: groupCode, number_of_students: 0, speciality_number: faker.random.number({min: 1, max: 29})};
        GroupsOnInsert.push(group);
    }
    await Group.bulkCreate(GroupsOnInsert, {validate: true});
    for(let i = 1; i <= 29; i++)
    {
        let groupsCount  = await Group.count({
            where: {
                speciality_number: i
            }
        });
        let studentsCount = await Group.sum('number_of_students', {where: {speciality_number: i}});
        await Speciality.update({number_of_groups: groupsCount, number_of_students: studentsCount}, {
            where: {
                speciality_number: i
            }
        });

    }
    const StudentsOnInsert = [];
    let prevStMaxId = await Student.max('student_id');
    let k = prevStMaxId + 1;
    for(let i = 0; i < quantity; i++)
    {
        let studentsInGroup = faker.random.number({min: 10, max: 30});
        await Group.update({number_of_students: studentsInGroup}, {
            where: {
              group_id: prevMaxId + i + 1 
            }
        });
        for(let j = 0; j < studentsInGroup; j++)
        {
            let student = {student_id: k, fullname: faker.name.firstName() + ' ' + faker.name.lastName(), age: faker.random.number({min: 18, max: 70}), group_id: prevMaxId + i + 1,
             average_mark: 0, graduate_score: faker.random.number({min: 120, max: 200})};
            StudentsOnInsert.push(student);
            k++;
        }
        if(StudentsOnInsert.length > 50000)
        {
            await Student.bulkCreate(StudentsOnInsert, {validate: true});
            while(StudentsOnInsert.length > 0)
            {
                StudentsOnInsert.pop();
            }
        }
    }
    await Student.bulkCreate(StudentsOnInsert, {validate: true});
    const TeachersOnInsert = [];
    const degrees = ['Instructor', 'Senior Instructor', 'Master Instructor', 'Assistant Professor', 'Associate Professor', 'University Professor'];
    prevTeacherMaxId = await Teacher.max('teacher_id');
    for(let i = 0; i < quantity * 2; i++)
    {
        let t_age = faker.random.number({min: 25, max: 80});
        let t_degree;
        if(t_age >= 25 && t_age < 29)
        {
            t_degree = degrees[0];
        }
        if(t_age > 28 && t_age < 32)
        {
            t_degree = faker.random.arrayElement(['Instructor', 'Senior Instructor']);
        }
        if(t_age > 31 && t_age < 36)
        {
            t_degree = faker.random.arrayElement(['Instructor', 'Senior Instructor', 'Master Instructor']);
        }
        if(t_age > 35 && t_age < 41)
        {
            t_degree = faker.random.arrayElement(['Senior Instructor', 'Master Instructor', 'Assistant Professor']);
        }
        if(t_age > 40 && t_age < 48)
        {
            t_degree = faker.random.arrayElement(['Senior Instructor', 'Master Instructor', 'Assistant Professor', 'Associate Professor']);
        }
        if(t_age > 47 && t_age < 56)
        {
            t_degree = faker.random.arrayElement(['Master Instructor', 'Assistant Professor', 'Associate Professor', 'University Professor']);
        }
        if(t_age > 55 && t_age < 61)
        {
            t_degree = faker.random.arrayElement(['Assistant Professor', 'Associate Professor', 'University Professor']);
        }
        if(t_age > 60 && t_age < 66)
        {
            t_degree = faker.random.arrayElement(['Associate Professor', 'University Professor']);
        }
        if(t_age > 65)
        {
            t_degree = degrees[5];
        }
        let teacher = {teacher_id: prevTeacherMaxId + i + 1, fullname: faker.name.firstName() + ' ' + faker.name.lastName(), age: t_age, academic_degree: t_degree, faculty_name: faker.random.arrayElement(FACULTY_NAMES)};
        console.log(`${teacher.teacher_id}\n${teacher.fullname}\n${teacher.age}\n${teacher.academic_degree}\n${teacher.faculty_name}`);
        TeachersOnInsert.push(teacher);  
    }
    await Teacher.bulkCreate(TeachersOnInsert, {validate: true});
    const TeacherSubOnInsert = [];
    for(let i = 0; i < TeachersOnInsert.length; i++)
    {
        let q = faker.random.number({min: 1, max: 3});
        let subject_ids = [];
        for(let j = 0; j < q; j++)
        {
            let s_id = faker.random.number({min: 1, max: 228});
            if(subject_ids.includes(s_id))
            {
                j--;
            }
            else
            {
                subject_ids.push(s_id);
            }
        }
        for(let k = 0; k < q; k++)
        {
            let ts = {teacher_id: prevTeacherMaxId + i + 1, subject_id: subject_ids[k]};
            TeacherSubOnInsert.push(ts);
        }
    }
    await TeacherSubjects.bulkCreate(TeacherSubOnInsert, {validate: true});
    const grTeachersOnInsert = [];
    let teachers = await Teacher.findAll({where:{teacher_id: {[Op.gt]: prevTeacherMaxId}}, raw: true});
    let teachersMap = new Map();
    for(let t of teachers)
    {
        teachersMap.set(t.teacher_id, 0);
    }  
    for(let i = 0; i < 228; i++)
    {
        let groups = await Group.findAll({
            where: {
                speciality_number: i + 1,
                group_id: {
                    [Op.gt]: prevMaxId
                }
            },
            raw: true
        });
        let subjects = await Subject.findAll({
            where: {
                speciality_number: i + 1
            },
            raw: true
        });
        let sub_ids = [];
        for(let sub of subjects)
        {
            sub_ids.push(sub.subject_id);
        }
        sub_ids.sort(function(a, b) { 
            return a - b;
        })
        let teacherPretSub = await TeacherSubjects.findAll({
            where: {
                subject_id: {
                    [Op.between]: [sub_ids[0], sub_ids[sub_ids.length - 1]]
                }
            },
            raw: true
        });
        let teacherSub = [];
        for(let t of teacherPretSub)
        {
            if(typeof(teachersMap.get(t.teacher_id)) == 'number')
            {
                console.log("+++");
                teacherSub.push(t);
            }
        }
        console.log(teacherSub);
        for(let gr of groups)
        {
            let sub_idsCopy = sub_ids.slice();
            while(sub_idsCopy.length != 0)
            {
                let teacherSubCopy = teacherSub.slice(faker.random.number({min: 0, max: teacherSub.length - 1}), teacherSub.length);
                for(let ts of teacherSubCopy)
                {
                    let index = sub_idsCopy.indexOf(ts.subject_id);
                    if(index != -1)
                    {
                        grTeachersOnInsert.push({teacher_id: ts.teacher_id, group_id: gr.group_id, subject_id: ts.subject_id});
                        sub_idsCopy.splice(index, 1);
                        let counter = teachersMap.get(ts.teacher_id);
                        if(counter == 11)
                        {
                            teachersMap.delete(ts.teacher_id);
                            teacherSub.splice(teacherSub.indexOf(ts.teacher_id), 1);
                        }
                        else
                        {
                            teachersMap.set(ts.teacher_id, counter + 1);
                        }
                        break;
                    }
                }
            }
        }
        if(grTeachersOnInsert.length > 20000)
        {
            await GroupTeachers.bulkCreate(grTeachersOnInsert, {validate: true});
            while(grTeachersOnInsert.length > 0)
            {
                grTeachersOnInsert.pop();
            }
        }
    }
    console.log(grTeachersOnInsert);
    await GroupTeachers.bulkCreate(grTeachersOnInsert, {validate: true});
    const MarksOnInsert = [];
    const limit_ = 50000;
    let offset_ = 0;
    let students = await Student.findAll({where: {average_mark: 0}, raw: true, limit: limit_});
    while(students.length != 0)
    {
        for(let st of students)
        {
            let st_marks = [];
            let gr = await Group.findAll({where: {group_id: st.group_id}, raw: true});
            let subjects = await Subject.findAll({where: {speciality_number: gr[0].speciality_number}, raw: true});
            for(let sub of subjects)
            {
                let m;
                let teacher = await GroupTeachers.findAll({where: {group_id: st.group_id, subject_id: sub.subject_id}, raw: true});
                m = 80;
                if(teacher.academic_degree == 'Senior Instructor')
                {
                    m = m * 1.01;
                }
                if(teacher.academic_degree == 'Master Instructor')
                {
                    m = m * 1.03;
                }
                if(teacher.academic_degree == 'Assistant Professor')
                {
                    m = m * 1.06;
                }
                if(teacher.academic_degree == 'Associate Professor')
                {
                    m = m * 1.08;
                }
                if(teacher.academic_degree == 'University Professor')
                {
                    m = m * 1.10;
                }
                let age = st.age;
                if(age < 36)
                {
                    m = m + m * age / 500;
                }
                else
                {
                    m = m - m * age / 1000;
                }
                if(st.graduate_score > 150)
                {
                    m = m * (1 + st.graduate_score / 1000);
                }
                else
                {
                    m = m * (0.85 + st.graduate_score / 1000);
                }
                if(m > 100)
                {
                    m = 100;
                }
                let m_str = m.toFixed(2);
                m = parseFloat(m_str);
                MarksOnInsert.push({mark: m, subject_id: sub.subject_id, student_id: st.student_id});
                st_marks.push(m);
            }
            let average = 0;
            let total = 0;
            for(let k = 0; k < st_marks.length; k++)
            {
                total = total + st_marks[k];
            }
            average = total / st_marks.length;
            let average_str = average.toFixed(2);
            average = parseFloat(average_str);
            await Student.update({average_mark: average}, {
                where: {
                    student_id: st.student_id
                }
            });
            if(MarksOnInsert.length > 100000)
            {
                await Mark.bulkCreate(MarksOnInsert, {validate: true});
                while(MarksOnInsert.length > 0)
                {
                    MarksOnInsert.pop();
                }
            }
        }
        offset_ = offset_ + limit_;
        students = await Student.findAll({where: {average_mark: 0}, raw: true, limit: limit_});
    }
    await Mark.bulkCreate(MarksOnInsert, {validate: true});
}

async function createRandomMark(st)
{
    let st_marks = [];
    let gr = await Group.findAll({where: {group_id: st.group_id}, raw: true});
    let subjects = await Subject.findAll({where: {speciality_number: gr[0].speciality_number}, raw: true});
    for(let sub of subjects)
    {
        let m;
        let grTeacher = await GroupTeachers.findAll({where: {group_id: st.group_id, subject_id: sub.subject_id}, raw: true});
        let teacher = await Teacher.findAll({where: {teacher_id: grTeacher[0].teacher_id}, raw: true});
        m = 80;
        if(teacher.academic_degree == 'Senior Instructor')
        {
            m = m * 1.01;
        }
        if(teacher.academic_degree == 'Master Instructor')
        {
            m = m * 1.03;
        }
        if(teacher.academic_degree == 'Assistant Professor')
        {
            m = m * 1.06;
        }
        if(teacher.academic_degree == 'Associate Professor')
        {
            m = m * 1.08;
        }
        if(teacher.academic_degree == 'University Professor')
        {
            m = m * 1.10;
        }
        let age = st.age;
        if(age < 36)
        {
            m = m + m * age / 500;
        }
        else
        {
            m = m - m * age / 1000;
        }
        if(st.graduate_score > 150)
        {
            m = m * (1 + st.graduate_score / 1000);
        }
        else
        {
            m = m * (0.85 + st.graduate_score / 1000);
        }
        if(m > 100)
        {
            m = 100;
        }
        let m_str = m.toFixed(2);
        m = parseFloat(m_str);
        st_marks.push(m);
        let total = 0;
        for(let k = 0; k < st_marks.length; k++)
        {
            total = total + st_marks[k];
        }
        let average = total / st_marks.length;
        let average_str = average.toFixed(2);
        average = parseFloat(average_str);
        await Student.update({average_mark: average}, {where: {student_id: st.student_id}});
        return {average_mark: average, subs: subjects};
    }
}

async function MakePrediction(st, subs)
{
    let data = JSON.parse(fs.readFileSync("trained-network-marks1.json"));
    const network = new brain.NeuralNetwork();
    network.fromJSON(data);
    let predictions = [];
    let gr = await Group.findAll({where: {group_id: st.group_id}, raw: true});
    let i = 0;
    for(let s of subs)
    {
        let grTeacher = await GroupTeachers.findAll({where: {group_id: st.group_id, subject_id: s.subject_id}, raw: true});
        let teacher = await Teacher.findAll({where: {teacher_id: grTeacher[0].teacher_id}, raw: true});
        let tg = 0;
                if(teacher[0].academic_degree == 'Senior Instructor')
                {
                    tg = 1;
                }
                 {
                    tg = 2;
                }
                if(teacher[0].academic_degree == 'Assistant Professor')
                {
                    tg = 3;
                }
                if(teacher[0].academic_degree == 'Associate Professor')
                {
                    tg = 4;
                }
                if(teacher[0].academic_degree == 'University Professor')
                {
                    tg = 5;
                }
        predictions.push(network.run({a: st.age / 100, b: st.graduate_score / 200, c: tg / 10}));
        console.log(teacher[0].academic_degree);
    }
    let total = 0;
    for(pr of predictions)
    {
        console.log(pr.d);
        let p = parseFloat(pr.d);
        p = p * 100;
        total = total + p;
    }
    let prediction = total / predictions.length;
    return prediction;
}

async function start()
{
    let dump_number = 1;
    while(true)
    {
        let input = readlineSync.question('Enter command:\n1 - to add new random data in database\n2 - to add your student\n3 to make plots\n4 - to make dump\n5 - to restore DB\n6 - to stop the programm');
        {
            if(input == 1)
            {
                let quantity = readlineSync.question('Enter number of groups(new students, teachers and marks will be created automatically)');
                addRandomData(quantity);
            }
            if(input == 2)
            {
                let st_id = await Student.max('student_id') + 1;
                let name = readlineSync.question('Enter student fullname: ');
                let st_age = readlineSync.question('Enter student age: ');
                let gr_id = faker.random.number({min: 1, max: await Group.max('group_id')});
                let grad_score = readlineSync.question('Enter student graduate score (from 120 to 200): ');
                let student = await Student.create({student_id: st_id, fullname: name, age: st_age, group_id: gr_id, average_mark: 0, graduate_score: grad_score});
                let mark = await createRandomMark(student);
                const prediction = await MakePrediction(student, mark.subs);
                console.log(`\nPredicted average mark: ${prediction}\n\nActual average mark: ${mark.average_mark}`);
            }
            if(input == 3)
            {
                let marks = [];
                for(let i = 18; i <= 70; i++)
                {
                    let students = await Student.findAll({where: {age: i, graduate_score: 150}, limit: 1000, raw: true});
                    for(let st of students)
                    {
                        let flag = 0;
                        let groups = await Group.findAll({where: {group_id: st.group_id}, raw: true});
                        let grTeachers = await GroupTeachers.findAll({where: {group_id: groups[0].group_id}, raw: true});
                        for(let t of grTeachers)
                        {
                            let teacher = await Teacher.findAll({where: {teacher_id: t.teacher_id}, raw: true});
                            if(teacher[0].academic_degree == 'Instructor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marks.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                        }
                        if(flag == 1)
                        {
                            break;
                        }
                    }
                }
                let ages = [];
                for(let j = 18; j <= 70; j++)
                {
                    ages.push(j);
                }
                const chart = new ChartJsImage();
                chart.setConfig({
                type: 'line',
                data: { labels: ages, datasets: [{ label: 'Dependency between mark and student`s age', data: marks}] },
                });
                chart.setWidth(1920).setHeight(1080);
                await chart.toFile('./MyPlots/AgesDependencyPlot.png');
                let scores = [];
                let marksOnScore = [];
                for(let i = 120; i <= 200; i++)
                {
                    scores.push(i);
                    let students = await Student.findAll({where: {graduate_score: i, age: 20}, limit: 1000, raw: true});
                    for(let st of students)
                    {
                        let flag = 0;
                        let groups = await Group.findAll({where: {group_id: st.group_id}, raw: true});
                        let grTeachers = await GroupTeachers.findAll({where: {group_id: groups[0].group_id}, raw: true});
                        for(let t of grTeachers)
                        {
                            let teacher = await Teacher.findAll({where: {teacher_id: t.teacher_id}, raw: true});
                            if(teacher[0].academic_degree == 'Instructor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnScore.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                        }
                        if(flag == 1)
                        {
                            break;
                        }
                    }
                }
                const chart2 = new ChartJsImage();
                chart.setConfig({
                type: 'line',
                data: { labels: scores, datasets: [{ label: 'Dependency between mark and student`s graduate score', data: marksOnScore}] },
                });
                chart.setWidth(1920).setHeight(1080);
                await chart.toFile('./MyPlots/ScoresDependencyPlot.png');
                let marksOnAcademicDeg = [];
                for(let i = 0; i < 6; i++)
                {
                    let students = await Student.findAll({where: {age: 20, graduate_score: 150}, limit: 1000, raw: true});
                    for(let st of students)
                    {
                        let flag = 0;
                        let groups = await Group.findAll({where: {group_id: st.group_id}, raw: true});
                        let grTeachers = await GroupTeachers.findAll({where: {group_id: groups[0].group_id}, raw: true});
                        for(let t of grTeachers)
                        {
                            let teacher = await Teacher.findAll({where: {teacher_id: t.teacher_id}, raw: true});
                            if(i == 0 && teacher[0].academic_degree == 'Instructor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnAcademicDeg.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                            if(i == 1 && teacher[0].academic_degree == 'Senior Instructor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnAcademicDeg.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                            if(i == 2 && teacher[0].academic_degree == 'Master Instructor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnAcademicDeg.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                            if(i == 3 && teacher[0].academic_degree == 'Assistant Professor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnAcademicDeg.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                            if(i == 4 && teacher[0].academic_degree == 'Associate Professor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnAcademicDeg.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                            if(i == 5 && teacher[0].academic_degree == 'University Professor')
                            {
                                let m = await Mark.findAll({where: {student_id: st.student_id, subject_id: t.subject_id}, raw: true});
                                marksOnAcademicDeg.push(m[0].mark);
                                flag = 1;
                                break;
                            }
                        }
                        if(flag == 1)
                        {
                            break;
                        }
                    }
                }
                const chart3 = new ChartJsImage();
                chart3.setConfig({
                type: 'line',
                data: { labels: ['Instructor', 'Senior Instructor', 'Master Instructor', 'Assistant Professor', 'Associate Professor', 'University Professor'],
                 datasets: [{ label: 'Dependency between mark and teacher`s academic degree', data: marksOnAcademicDeg}] },
                });
                chart3.setWidth(1920).setHeight(1080);
                await chart3.toFile('./MyPlots/AcademicDegreeDependencyPlot.png');
                let students = await Student.findAll({limit: 150, raw: true});
                let actuallMarks = [];
                let predictedMarks = [];
                let labelsArr = [];
                let i = 0;
                for(let st of students)
                {
                    actuallMarks.push(st.average_mark);
                    let groups = await Group.findAll({where: {group_id: st.group_id}, raw: true});
                    let spec = await Speciality.findAll({where: {speciality_number: groups[0].speciality_number}, raw: true});
                    let subs = await Subject.findAll({where: {speciality_number: spec[0].speciality_number}, raw: true});
                    predictedMarks.push(await MakePrediction(st, subs));
                    i++;
                    labelsArr.push(i);
                }
                const chart4 = new ChartJsImage();
                chart4.setConfig({
                type: 'line',
                data: { labels: labelsArr, 
                 datasets: [{ label: 'Actuall Marks', data: actuallMarks},
                {label: 'Predicted Marks', data: predictedMarks}]},
                });
                chart4.setWidth(1920).setHeight(1080);
                await chart4.toFile('./MyPlots/Actuall-PredictionCharts.png');

            }
            if(input == 4)
            {
                exec(`mkdir ./Databases/Coursework/Backups/backup${dump_number}`, {cwd: '/home/kyrychenko11'}, (error, stdout, stderr) => {

                    if (error) {
                  
                      console.error(`exec error: ${error}`);
                  
                      return;
                    }
                    console.log(`stdout: ${stdout}`);
                  
                    console.log(`stderr: ${stderr}`);
                  });
                  const start= new Date().getTime();
                exec(`pg_dump -U postgres -Fd Students -f ./Databases/Coursework/Backups/backup${dump_number}`, {cwd: '/home/kyrychenko11'}, (error, stdout, stderr) => {

                    if (error) {
                  
                      console.error(`exec error: ${error}`);
                  
                      return;
                    }
                    console.log(`stdout: ${stdout}`);
                  
                    console.log(`stderr: ${stderr}`);
                  });
                  const end = new Date().getTime();
                  console.log(`Dumped! Exucution time: ${end - start}ms`);
                dump_number++;
            }
            if(input == 5)
            {
                let n = readlineSync.question('Enter 1 - to restore from the latest dump, 2 - to restore from another dump');
                if(n == 1)
                {
                    
                    exec(`pg_restore -U postgres -Fd -d Students ./Databases/Coursework/Backups/backup${dump_number - 1}`, {cwd: '/home/kyrychenko11'}, (error, stdout, stderr) => {

                        if (error) {
                      
                          console.error(`exec error: ${error}`);
                      
                          return;
                        }
                        console.log(`stdout: ${stdout}`);
                      
                        console.log(`stderr: ${stderr}`);
                      });
                }
                if(n == 2)
                {
                    let d_nStr = readlineSync.question('Enter dump number: ');
                    let d_n = parseInt(d_nStr);
                    exec(`pg_restore -U postgres -Fd -d Students ./Databases/Coursework/Backups/backup${d_n}`, {cwd: '/home/kyrychenko11'}, (error, stdout, stderr) => {
                        if (error) {
                      
                          console.error(`exec error: ${error}`);
                      
                          return;
                        }
                        console.log(`stdout: ${stdout}`);
                      
                        console.log(`stderr: ${stderr}`);
                      });
                }
            }
            if(input == 6)
            {
                break;
            }
        }
    }
}
start();