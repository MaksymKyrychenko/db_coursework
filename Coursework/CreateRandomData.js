const {Faculty,Student,Speciality,Group,Subject,Teacher,Mark,TeacherSubjects, GroupTeachers} = require('./Models');

const faker = require('faker/locale/en_US');
const fs = require('fs');

const  {Sequelize, Model, DataTypes, Op} = require('sequelize');
const sequelize = new Sequelize('Students', 'postgres', 'Max095433', {
    host: 'localhost',
    dialect: 'postgres'
});

const FACULTY_NAMES = ['School of Architecture and Planning', 'School of Engineering', 'School of Humanities, Arts, and Social Sciences',
'Sloan School of Management', 'School of Science'];

const SPECIALITY_NAMES = ['Architecture', 'Media Arts and Sciences', 'Urban Studies and Planning',
'Aeronautics and Astronautics', 'Biological Engineering', 'Chemical Engineering', 'Civil and Environmental Engineering',
'Data, Systems, and Society', 'Electrical Engineering and Computer Science', 'Materials Science and Engineering',
'Mechanical Engineering', 'Medical Engineering and Science', 'Nuclear Science and Engineering', 
'Anthropology', 'Comparative Media', 'Economics', 'Global Studies and Languages', 'History', 'Humanities', 'Linguistics',
'Literature', 'Music and Theater Arts', 'Philosophy', 'Political Science', 'Management', 'Biology', 'Chemistry', 'Mathematics', 'Physics'];

async function CreateRandomData(quantity)
{
    const Faculties = [];
    for(let i = 0; i < FACULTY_NAMES.length; i++)
    {
        let faculty = {faculty_name: FACULTY_NAMES[i], number_of_specialities: 0};
        Faculties.push(faculty);
    }
    await Faculty.bulkCreate(Faculties, {validate: true});
    const Specialities = [];
    for(let i = 0; i < SPECIALITY_NAMES.length; i++)
    {
        let facName;
        if(i < 3)
        {
            facName = FACULTY_NAMES[0];
        }
        if(i >= 3 && i < 13)
        {
            facName = FACULTY_NAMES[1];
        }
        if(i >= 13 && i < 24)
        {
            facName = FACULTY_NAMES[2];
        }
        if(i == 24)
        {
            facName = FACULTY_NAMES[3];
        }
        if(i > 24)
        {
            facName = FACULTY_NAMES[4];
        }
        let speciality = {speciality_number: i + 1, speciality_name: SPECIALITY_NAMES[i], number_of_groups: 0, number_of_students: 0, faculty_name: facName};
        Specialities.push(speciality);
    }
    await Speciality.bulkCreate(Specialities, {validate: true});
    for(let i = 0; i < FACULTY_NAMES.length; i++)
    {
        let specCount = await Speciality.count({
            where: {
                faculty_name: FACULTY_NAMES[i]
            }
        });
        await Faculty.update({number_of_specialities: specCount}, {where: {faculty_name: FACULTY_NAMES[i]}});
    }
    const SubjectsOnInsert = [];
    const jsonText = fs.readFileSync('./subjects.json');
    const Subjects = JSON.parse(jsonText);
    for(let i = 0; i < Subjects.length; i++)
    {
        let subject = {subject_id: Subjects[i].id, title: Subjects[i].title, credits: (faker.random.number(11) + 1), speciality_number: Subjects[i].spec};
        SubjectsOnInsert.push(subject);
    }
    await Subject.bulkCreate(SubjectsOnInsert, {validate: true});
    const GroupsOnInsert = [];
    for(let i = 0; i < quantity; i++)
    {
        let groupCode = faker.random.alpha({count: 2, upcase: true}) + ' - ' + faker.random.number({min: 101, max: 499});
        let group = {group_id: i + 1, group_name: groupCode, number_of_students: 0, speciality_number: faker.random.number({min: 1, max: SPECIALITY_NAMES.length})};
        GroupsOnInsert.push(group);
    }
    await Group.bulkCreate(GroupsOnInsert, {validate: true});
    for(let i = 1; i <= SPECIALITY_NAMES.length; i++)
    {
        let groupsCount  = await Group.count({
            where: {
                speciality_number: i
            }
        });
        let studentsCount = await Group.sum('number_of_students', {where: {speciality_number: i}});
        await Speciality.update({number_of_groups: groupsCount, number_of_students: studentsCount}, {
            where: {
                speciality_number: i
            }
        });

    }
    const StudentsOnInsert = [];
    let k = 1;
    for(let i = 0; i < quantity; i++)
    {
        let studentsInGroup = faker.random.number({min: 10, max: 30});
        await Group.update({number_of_students: studentsInGroup}, {
            where: {
              group_id: i + 1 
            }
        });
        for(let j = 0; j < studentsInGroup; j++)
        {
            let student = {student_id: k, fullname: faker.name.firstName() + ' ' + faker.name.lastName(), age: faker.random.number({min: 18, max: 70}), group_id: i + 1,
             average_mark: 0, graduate_score: faker.random.number({min: 120, max: 200})};
            StudentsOnInsert.push(student);
            k++;
        }
        if(StudentsOnInsert.length > 50000)
        {
            await Student.bulkCreate(StudentsOnInsert, {validate: true});
            while(StudentsOnInsert.length > 0)
            {
                StudentsOnInsert.pop();
            }
        }
    }
    await Student.bulkCreate(StudentsOnInsert, {validate: true});
    const TeachersOnInsert = [];
    const degrees = ['Instructor', 'Senior Instructor', 'Master Instructor', 'Assistant Professor', 'Associate Professor', 'University Professor'];
    for(let i = 0; i < quantity * 2; i++)
    {
        let t_age = faker.random.number({min: 25, max: 80});
        let t_degree;
        if(t_age >= 25 && t_age < 29)
        {
            t_degree = degrees[0];
        }
        if(t_age > 28 && t_age < 32)
        {
            t_degree = faker.random.arrayElement(['Instructor', 'Senior Instructor']);
        }
        if(t_age > 31 && t_age < 36)
        {
            t_degree = faker.random.arrayElement(['Instructor', 'Senior Instructor', 'Master Instructor']);
        }
        if(t_age > 35 && t_age < 41)
        {
            t_degree = faker.random.arrayElement(['Senior Instructor', 'Master Instructor', 'Assistant Professor']);
        }
        if(t_age > 40 && t_age < 48)
        {
            t_degree = faker.random.arrayElement(['Senior Instructor', 'Master Instructor', 'Assistant Professor', 'Associate Professor']);
        }
        if(t_age > 47 && t_age < 56)
        {
            t_degree = faker.random.arrayElement(['Master Instructor', 'Assistant Professor', 'Associate Professor', 'University Professor']);
        }
        if(t_age > 55 && t_age < 61)
        {
            t_degree = faker.random.arrayElement(['Assistant Professor', 'Associate Professor', 'University Professor']);
        }
        if(t_age > 60 && t_age < 66)
        {
            t_degree = faker.random.arrayElement(['Associate Professor', 'University Professor']);
        }
        if(t_age > 65)
        {
            t_degree = degrees[5];
        }
        let teacher = {teacher_id: i + 1, fullname: faker.name.firstName() + ' ' + faker.name.lastName(), age: t_age, academic_degree: t_degree, faculty_name: faker.random.arrayElement(FACULTY_NAMES)};
        TeachersOnInsert.push(teacher);  
    }
    await Teacher.bulkCreate(TeachersOnInsert, {validate: true});
    const TeacherSubOnInsert = [];
    for(let i = 0; i < TeachersOnInsert.length; i++)
    {
        let q = faker.random.number({min: 1, max: 3});
        let subject_ids = [];
        for(let j = 0; j < q; j++)
        {
            let s_id = faker.random.number({min: 1, max: Subjects.length});
            if(subject_ids.includes(s_id))
            {
                j--;
            }
            else
            {
                subject_ids.push(s_id);
            }
        }
        for(let k = 0; k < q; k++)
        {
            let ts = {teacher_id: i + 1, subject_id: subject_ids[k]};
            TeacherSubOnInsert.push(ts);
        }
    }
    await TeacherSubjects.bulkCreate(TeacherSubOnInsert, {validate: true});
    const grTeachersOnInsert = [];
    let teachers = await Teacher.findAll({raw: true});
    let teachersMap = new Map();
    for(let t of teachers)
    {
        teachersMap.set(t.teacher_id, 0);
    }  
    for(let i = 0; i < SPECIALITY_NAMES.length; i++)
    {
        let groups = await Group.findAll({
            where: {
                speciality_number: i + 1
            },
            raw: true
        });
        let subjects = await Subject.findAll({
            where: {
                speciality_number: i + 1
            },
            raw: true
        });
        let sub_ids = [];
        for(let sub of subjects)
        {
            sub_ids.push(sub.subject_id);
        }
        let teacherPretSub = await TeacherSubjects.findAll({
            where: {
                subject_id: {
                    [Op.between]: [Math.min.apply(null, sub_ids), Math.max.apply(null, sub_ids)]
                }
            },
            raw: true
        });
        let teacherSub = [];
        for(let t of teacherPretSub)
        {
            if(typeof (teachersMap.get(t.teacher_id)) == 'number')
            {
                console.log("+++");
                teacherSub.push(t);
            }
        }
        console.log(teacherSub);
        for(let gr of groups)
        {
            let sub_idsCopy = sub_ids.slice();
            while(sub_idsCopy.length != 0)
            {
                let teacherSubCopy = teacherSub.slice(faker.random.number({min: 0, max: teacherSub.length - 1}), teacherSub.length);
                for(let ts of teacherSubCopy)
                {
                    let index = sub_idsCopy.indexOf(ts.subject_id);
                    if(index != -1)
                    {
                        grTeachersOnInsert.push({teacher_id: ts.teacher_id, group_id: gr.group_id, subject_id: ts.subject_id});
                        sub_idsCopy.splice(index, 1);
                        let counter = teachersMap.get(ts.teacher_id);
                        if(counter == 11)
                        {
                            teachersMap.delete(ts.teacher_id);
                            teacherSub.splice(teacherSub.indexOf(ts.teacher_id), 1);
                        }
                        else
                        {
                            teachersMap.set(ts.teacher_id, counter + 1);
                        }
                        break;
                    }
                }
            }
        }
        if(grTeachersOnInsert.length > 20000)
        {
            await GroupTeachers.bulkCreate(grTeachersOnInsert, {validate: true});
            while(grTeachersOnInsert.length > 0)
            {
                grTeachersOnInsert.pop();
            }
        }
    }
    console.log(grTeachersOnInsert);
    await GroupTeachers.bulkCreate(grTeachersOnInsert, {validate: true});
    const MarksOnInsert = [];
    const limit_ = 50000;
    let offset_ = 0;
    let students = await Student.findAll({raw: true, offset: offset_, limit: limit_});
    while(students.length != 0)
    {
        for(let st of students)
        {
            let st_marks = [];
            let gr = await Group.findAll({where: {group_id: st.group_id}, raw: true});
            console.log(gr[0].speciality_number);
            let subjects = await Subject.findAll({where: {speciality_number: gr[0].speciality_number}, raw: true});
            for(let sub of subjects)
            {
                let grTeacher = await GroupTeachers.findAll({where: {group_id: st.group_id, subject_id: sub.subject_id}, raw: true});
                let m = 80;
                let teacher = await Teacher.findAll({where: {teacher_id: grTeacher[0].teacher_id}});
                if(teacher[0].academic_degree == 'Senior Instructor')
                {
                    m = m * 1.01;
                }
                if(teacher[0].academic_degree == 'Master Instructor')
                {
                    m = m * 1.03;
                }
                if(teacher[0].academic_degree == 'Assistant Professor')
                {
                    m = m * 1.06;
                }
                if(teacher[0].academic_degree == 'Associate Professor')
                {
                    m = m * 1.08;
                }
                if(teacher[0].academic_degree == 'University Professor')
                {
                    m = m * 1.10;
                }
                let age = st.age;
                if(age < 36)
                {
                    m = m + m * age / 500;
                }
                else
                {
                    m = m - m * age / 1000;
                }
                if(st.graduate_score > 150)
                {
                    m = m * (1 + st.graduate_score / 1000);
                }
                else
                {
                    m = m * (0.85 + st.graduate_score / 1000);
                }
                if(m > 100)
                {
                    m = 100;
                }
                let m_str = m.toFixed(2);
                m = parseFloat(m_str);
                MarksOnInsert.push({mark: m, subject_id: sub.subject_id, student_id: st.student_id});
                st_marks.push(m);
            }
            let average = 0;
            let total = 0;
            for(let k = 0; k < st_marks.length; k++)
            {
                total = total + st_marks[k];
            }
            average = total / st_marks.length;
            let average_str = average.toFixed(2);
            average = parseFloat(average_str);
            await Student.update({average_mark: average}, {
                where: {
                    student_id: st.student_id
                }
            });
            if(MarksOnInsert.length > 100000)
            {
                await Mark.bulkCreate(MarksOnInsert, {validate: true});
                while(MarksOnInsert.length > 0)
                {
                    MarksOnInsert.pop();
                }
            }
        }
        offset_ = offset_ + limit_;
        students = await Student.findAll({raw: true, offset: offset_, limit: limit_});
    }
    await Mark.bulkCreate(MarksOnInsert, {validate: true});
}

module.exports = {CreateRandomData};