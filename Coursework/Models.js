
const {Sequelize, DataTypes, Model} = require('sequelize');
const sequelize = new Sequelize('Students', 'postgres', 'Max095433', {
    host: 'localhost',
    dialect: 'postgres'
});

class Student extends Model {}
class Faculty extends Model {}
class Speciality extends Model {}
class Group extends Model {}
class Subject extends Model {}
class Teacher extends Model {}
class Mark extends Model {}
class GroupTeachers extends Model {}
class TeacherSubjects extends Model {}

Faculty.init({
    faculty_name: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },
    number_of_specialities: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
}, {
    sequelize, 
    modelName: 'Faculty',
    timestamps: false
});

Speciality.init({
    speciality_number: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    speciality_name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    number_of_students: {
type: DataTypes.INTEGER,
        allowNull: true
    },
    number_of_groups: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    faculty_name: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
            model: Faculty,
            key: 'faculty_name'
        }
    }
}, {
    sequelize,
    modelName: 'Speciality',
    timestamps: false
});

Group.init({
    group_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false
    },
    group_name: {
       type: DataTypes.STRING,
allowNull: false 
    },
    number_of_students: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    speciality_number: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Speciality,
            key: 'speciality_number'
        }
    }
}, {
    sequelize,
    modelName: 'Group',
    timestamps: false
});
Subject.init({
    subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false 
    },
    credits: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    speciality_number: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Speciality,
            key: 'speciality_number'
        }
    }
}, {
    sequelize,
    modelName: 'Subject',
timestamps: false
});

Teacher.init({
    teacher_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
    },
    fullname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false 
    },
    academic_degree: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: 'Teacher',
    timestamps: false
});
Student.init({
    student_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false 
    },
    group_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Group,
            key: 'group_id'
        }
    },
    average_mark: 
    {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    graduate_score: {
        type: DataTypes.DOUBLE,
        allowNull: false 
    } 
}, {
    sequelize,
    modelName: 'Student',
timestamps: false
});


Mark.init({
    mark: {
        type: DataTypes.DOUBLE,
        primaryKey: true,
        allowNull: false
    },
    subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        references: {
            model: Subject,
            key: 'subject_id'
        }
    },
    student_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false
}
}, {
    sequelize,
    modelName: 'Mark',
    timestamps: false
});

GroupTeachers.init({
    teacher_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        references: {
            model: Teacher,
            key: 'teacher_id'
        }
    },
    group_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        references: {
            model: Group,
            key: 'group_id'
        }
    },
    subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        references: {
            model: Subject,
            key: 'subject_id'
        }
    }
}, {
    sequelize,
    modelName: 'Teacher/Group',
    freezeTableName: true,
    timestamps: false
});


TeacherSubjects.init({
    teacher_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        references: {
            model: Teacher,
            key: 'teacher_id'
        }
    },
    subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
references: {
            model: Subject,
            key: 'subject_id'
        }
    }
}, {
    sequelize,
    modelName: 'Teacher/Subject',
    freezeTableName: true,
    timestamps: false
});

module.exports = {
    Faculty,
    Student,
    Speciality,
    Group,
    Subject,
    Teacher,
    Mark,
    TeacherSubjects,
    GroupTeachers
};